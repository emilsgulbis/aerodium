@extends('layouts.app')

@section('content')
    <section class="banner fullscreen" style="background-image:url('images/home-video-placeholder.jpg')">
        <div class="container banner__content">
            <h1 class="banner__heading m-b-lg h1"><strong>Wind tunnels</strong><br/>made for passion</h1>
            <a href="#" class="btn btn-primary">See our <strong>wind tunnels</strong></a>
        </div>
        <a href="#your-passion" class="fullscreen__scroll"><img src="images/scroll-down-en.png" alt="Scroll Down"></a>
        <video muted loop autoplay>
            <source src="video/aerodium.mp4" type="video/mp4">
        </video>
    </section>
    <section class="title title--primary parallax" id="your-passion">
        <img src="images/parallax-bg2.png" class="parallax__image" alt="" data-intensity="10">
        <div class="container">
            <h2 class="heading h1">What is your <strong>passion</strong>?</h2>
        </div>
    </section>
    <section class="row no-gutters">
        <div class="col-md-4">
            <a href="#"  class=" banner banner--darker banner--animate" style="background-image:url('images/category-business.jpg')">
                <h3 class="banner__heading h1"><strong>Business</strong></h3>
                <p class="banner__text">Outdoor or indoor Wind Tunnel model for bla bla</p>
            </a>
        </div>

        <div class="col-md-4">
            <a href="" class="banner banner--darker banner--animate" style="background-image:url('images/category-shows-and-events.jpg')">
                <h3 class="banner__heading h1"><strong>Shows</strong><br>and events</h3>
                <p class="banner__text">Wind Tunnel with glass walls for full Indoor sky-diving fun experience</p>
            </a>
        </div>

        <div class="col-md-4">
            <a href="#" class="banner banner--darker banner--animate" style="background-image:url('images/category-military.jpg')">
                <h3 class="banner__heading h1"><strong>Military</strong></h3>
                <p class="banner__text">Most powerful Wind Tunnel for profesional SkyDiving experience</p>
            </a>
        </div>
    </section>
    <section class="parallax">
        <img src="images/parallax-bg1.jpg" class="parallax__image" alt="">
        {{-- <div class="container"> --}}
            <div class="products">
                <div class="title">
                    <h2 class="h1">Our passion is <strong>innovations</strong></h2>
                    <p>We made most innovative Wind tunnels on the World</p>
                </div>
                <div class="row">
                    <div class="col-md-4 d-flex">
                        @component('components.product', array('image' => 'images/product-1.png'))
                            @slot('title')
                                <strong>Open</strong> wind tunnel
                            @endslot
                            @slot('description')
                                Outdoor or indoor Wind Tunnel model for bla bla
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-md-4 d-flex">
                        @component('components.product', array('image' => 'images/product-2.png', 'badge' => 'TOP #1'))
                            @slot('title')
                                <strong>Wall to wall</strong><br/> wind tunnel
                            @endslot
                            @slot('description')
                                Wind Tunnel with glass walls for full Indoor sky-diving fun experience
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-md-4 d-flex">
                        @component('components.product', array('image' => 'images/product-3.png'))
                            @slot('title')
                                <strong>Recirculation</strong> wind tunnel
                            @endslot
                            @slot('description')
                                Most powerful Wind Tunnel for profesional SkyDiving experience
                            @endslot
                        @endcomponent
                    </div>
                </div>
            </div>
        {{-- </div> --}}
    </section>
    <section class="title title--primary parallax">
        <img src="images/parallax-bg2.png" class="parallax__image" alt="">
        <div class="container">
            <h2 class="heading h1">We are <strong>global</strong></h2>
            <a href="#" class="btn btn-default btn--right">See our projects</a>
        </div>
    </section>
    @include('components.google-maps')
    <section class="title title--primary parallax">
        <img src="images/parallax-bg3.jpg" class="parallax__image" alt="">
        <div class="container">
            <h2 class="heading h1">Want to know us <strong>better</strong>?</h2>
            <a href="#" class="btn btn-default btn-alt">Contact Us</a>
        </div>
    </section>
@endsection
