<google-maps center="56.859996,24.616421" :zoom="9">
    <maps-marker position="56.859996,24.616421" icon="images/map-marker.png">
        <a href="#" class="maps__overlay">
            <div class="overlay__image" style="background-image:url('images/category-business.jpg')"></div>
            <div class="overlay__content">
                <h3 class="overlay__title">Lorem ipsum is simply dummy text</h3>
                <p class="overlay__position">
                    <span>Argentina</span>
                    <i class="ion-ios-location"></i>
                </p>
            </div>
        </a>
    </maps-marker>

    <maps-marker position="56.835964,23.023403" icon="images/map-marker.png">
        <a href="#" class="maps__overlay">
            <div class="overlay__image" style="background-image:url('images/category-business.jpg')"></div>
            <div class="overlay__content">
                <h3 class="overlay__title">Lorem ipsum is simply dummy text</h3>
                <p class="overlay__position">
                    <span>Argentina</span>
                    <i class="ion-ios-location"></i>
                </p>
            </div>
        </a>
    </maps-marker>
</google-maps>
