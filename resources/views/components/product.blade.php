<div class="product">
    <div class="product__content">
        <div class="product__image">

            <a href="#">
                <div class="img">
                    <img src="{{$image}}" alt="" class="img-fluid">
                    @if(isset($badge))
                    <div class="badge">{{$badge}}</div>
                    @endif
                </div>
            </a>
        </div>
        <h3 class="product__title"><a href="#">{{$title}}</a></h3>
        <div class="product__description">
            <p>{{$description}}</p>
        </div>
    </div>
    <a href="#" class="btn btn-primary">Learn more</a>
</div>
