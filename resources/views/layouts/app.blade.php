<!doctype html>
<html>
@include('layouts.head')
<body>
    @include('layouts.header')
    <div id="app">
        @yield('content')
    </div>
    @include('layouts.footer')
</body>
</html>
