<footer id="footer" style="background-image:url(images/footer-bg.jpg)">
    <div class="container">
        <div class="text-center">
            <ul class="nav nav-primary">
                <li class="nav-item"><a class="nav-link" href="#">Wind Tunnels</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Shows</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Flying</a></li>
                <li class="nav-item"><a class="nav-link" href="#">About Us</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Contact Us</a></li>
            </ul>
        </div>

        <div class="text-center">
            <ul class="nav nav-social">
                <li class="nav-item">
                    <a href="#" target="_blank" class="nav-link bg-facebook">
                        <i class="ion-social-facebook"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="nav-link bg-twitter">
                        <i class="ion-social-twitter"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="nav-link bg-youtube">
                        <i class="ion-social-youtube"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="nav-link bg-linkedin">
                        <i class="ion-social-linkedin"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="nav-link bg-whatsapp">
                        <i class="ion-social-whatsapp-outline"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="nav-link">
                        <i class="ion-paper-airplane"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="copyright">
            <p>Copyright &copy; Aerodium Technologies</p>
            <ul class="nav nav-dashed">
                <li class="nav-item"><a class="nav-link" href="#">Vacancies</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Faq</a></li>
            </ul>
            <p id="born"><span>Website made by</span> <a href="http://born.lv" target="_blank"><img src="images/born-logo.png" alt="BORN"></a></p>
        </div>
    </div>
</footer>

<script>var google_maps = {api_url : 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCdI1lqE55WoGwNTczfP_923QBZGQSvYcA&libraries=places&ver=3&callback=initMap'}</script>
<script src="{{mix('/js/main.js')}}"></script>
