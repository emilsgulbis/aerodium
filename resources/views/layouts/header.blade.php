<header id="header">
    <a href="/" class="brand">
        <img src="images/aerodium-logo.png" alt="Aerodium" width="132" height="174">
        <img src="images/aerodium-logo-hover.png" class="brand-hover" alt="Aerodium" width="132" height="174">
    </a>
    <div class="header--right">
        <nav class="drop">
            <div class="drop__head">
                <span class="drop__head--pre">
                    <i class="ion-ios-world-outline"></i>
                </span>
                <a href="">English</a>
                <span class="drop__head--arrow">
                    <i class="ion-ios-arrow-down"></i>
                </span>
            </div>
            <ul class="drop__dropdown">
                <li><a href="">Latviski</a></li>
                <li><a href="">English</a></li>
                <li><a href="">Русский </a></li>
            </ul>
        </nav>
        <div class="header__menu">
            <ul class="nav nav-primary">
                <li class="nav-item"><a class="nav-link" href="#" title="Wind Tunnels">Wind Tunnels</a></li>
                <li class="nav-item"><a class="nav-link" href="#" title="Shows">Shows</a></li>
                <li class="nav-item"><a class="nav-link" href="#" title="Flying">Flying</a></li>
                <li class="nav-item"><a class="nav-link" href="#" title="About Us">About Us</a></li>
                <li class="nav-item"><a class="nav-link" href="#" title="Contact Us">Contact Us</a></li>
            </ul>
            <div class="burger">
                <div></div>
            </div>
        </div>
    </div>
</header>
