
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.bus = new Vue();

// import {MapOverlay} from './components/MapOverlay.js';
Vue.component('google-maps', require('./components/Maps.vue'));
Vue.component('maps-marker', require('./components/MapsMarker.vue'));

const app = new Vue({
    el: '#app'
});

window.initMap = function(){
    bus.$emit('google:loaded');
    // MapOverlay.prototype = new google.maps.OverlayView();
};


var p = new Parallax('.parallax .parallax__image').init()

$(".fullscreen__scroll").on('click', function() {
    var href = $(this).attr('href');
    if($(href).length){
        $('html, body').animate({
            scrollTop: $(href).offset().top
        }, 600);
    }
});

$(window).on('scroll', function(){
    if($(window).scrollTop() > 50){
        $('#header').addClass('scrolled')
    }
    else{
        $('#header').removeClass('scrolled')
    }
})
